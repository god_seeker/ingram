export const log = (message, extra) => {
  if (extra) {
    return console.log(message, extra);
  }
  return console.log(message);
};
