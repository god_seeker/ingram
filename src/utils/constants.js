import { LRU } from "./LRU";

const NOAA_API = "https://www.ncdc.noaa.gov/cdo-web/api/v2/data";

const showDateFormat = "YYYY MMM";
const dateFormat = "YYYY-MM-DD";

const lruCache = new LRU();

export { NOAA_API, showDateFormat, dateFormat, lruCache };
