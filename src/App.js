import "./App.css";
import Home from "./pages/home";

function App() {
  return (
    <div className="App">
      <div className="header">
        <span className="title">InGram</span>
      </div>
      <div className="content">
        <Home />
      </div>
      <div className="footer">
        <span>&copy; 2021 Authority</span>
      </div>
    </div>
  );
}

export default App;
