import { render, screen } from "@testing-library/react";
import { rest } from "msw";
import { setupServer } from "msw/node";

import App from "./App";

import { NOAA_API } from "./utils/constants";

const server = setupServer(
  rest.get(NOAA_API, (req, res, ctx) => {
    return res(ctx.json({ greeting: "hello there" }));
  })
);

beforeAll(() => server.listen());
afterEach(() => server.resetHandlers());
afterAll(() => server.close());

test("render app with header and footer", () => {
  render(<App />);
  const linkElement = screen.getByText(/ingram/i);
  expect(linkElement).toBeInTheDocument();
});
