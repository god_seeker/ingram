import { useEffect, useState } from "react";

import { DatePicker, Spin } from "antd";

import { get } from "./../api-handler/requests";
import { Colors, LineChart } from "./../utils/charts";
import { log } from "./../utils/logger";
import {
  lruCache,
  dateFormat,
  showDateFormat,
  NOAA_API,
} from "./../utils/constants";

import moment from "moment";

import "./home.css";

function Home(props) {
  const [error, setError] = useState(null);
  const [chartData, setChartData] = useState();
  const [month, setMonth] = useState(moment().subtract(1, "month"));
  const [loading, setLoading] = useState(false);

  const onChange = (e) => {
    setMonth(e);
  };

  const location = "Abu Dhabi, AE"

  useEffect(() => {
    const start = moment(month).startOf("month").format(dateFormat);
    const end = moment(month).endOf("month").format(dateFormat);
    const url = `${NOAA_API}?datasetid=GHCND&locationid=CITY:AE000001&startdate=${start}&enddate=${end}`;

    const fecthData = () => {
      setLoading(true);
      get(url)
        .then((data) => {
          log(data);
          const reduced = data.results.reduce((pre, cur) => {
            if (!pre[cur.datatype]) {
              pre[cur.datatype] = [];
            }
            pre[cur.datatype].push(cur);
            return pre;
          }, {});
          let datasets = [];
          let colors = Colors;

          for (let key in reduced) {
            let color = colors.shift();

            datasets.push({
              label: key,
              data: reduced[key].map((o) => [o.date, o.value]),
              borderColor: color,
              backgroundColor: color,
            });

            colors.push(color);
          }

          log(datasets);
          lruCache.write(url, {
            datasets: datasets,
          });
          setChartData({
            datasets: datasets,
          });
        })
        .catch((e) => {
          setError("No Data");
        })
        .finally(() => setLoading(false));
    };

    let checkInCache = lruCache.read(url);
    if (checkInCache) {
      log("loaded from cache");
      setChartData(checkInCache);
    } else {
      fecthData();
    }
  }, [month]);

  return (
    <>
      <div className="controls">
        <span></span>

        <span>
          <DatePicker
            id="monthPicker"
            value={month}
            format={showDateFormat}
            onChange={onChange}
            picker="month"
            allowClear={false}
          />
        </span>
      </div>
      <div id="chartHolder" className="chart-holder">
        {loading ? <Spin tip="Loading..." /> : ""}

        {!chartData ? (
          <span id="errorData" className="nodata">{loading ? "" : error}</span>
        ) : (
          <LineChart id="lineChart" data={chartData} title={location}/>
        )}
      </div>
    </>
  );
}

export default Home;
